﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Diagnostics;
using System.Linq;
using MegoTravelTestTask.Services;
using NLog;
using System.Reflection;
using System.Threading;

namespace MegoTravelTestTask.Controllers
{
    public class SearchController : ApiController
    {
        public const int MaxWaitMs = 11000;

        // todo: добавить зависимость на HttpClient в конструктор
        private static readonly HttpClient HttpClient = new HttpClient();

        // todo: добавить зависимость на IMetricsCollector в конструктор
        private readonly IMetricsCollector _task3MetricsCollector = new MetricsCollector();

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public void Post([FromBody]string query)
        {
            WriteDebug($"{Assembly.GetExecutingAssembly().Location}");

            WriteDebug("");
            WriteDebug("=== Search Start ===");

            var generalStopwatch = new Stopwatch();

            var cts = new CancellationTokenSource();

            generalStopwatch.Start();

            var taskUrls = new int[] { 1, 2, 3, 4, 5 }
                .Select(i => Url.Link("request", new { id = i }))
                .ToArray();

            var task1 = ConfigureTask1(taskUrls[0], cts.Token);

            var task2 = ConfigureTask2(taskUrls[1], cts.Token);
            
            var task3 = ConfigureTask3(taskUrls[2], cts.Token);

            var task4 = ConfigureTask4And5(taskUrls[3], taskUrls[4], cts.Token);

            var tasks = new Task[] { task1, task2, task3, task4 };

            Task.WhenAll(tasks).Wait(MaxWaitMs);

            cts.Cancel();

            generalStopwatch.Stop();

            WriteDebug($"Search elapsed: {generalStopwatch.ElapsedMilliseconds}ms");

            WriteDebug("=== Search End ===");
        }

        private Task ConfigureTask1(string requestUrl, CancellationToken ct)
        {
            return HttpClient.GetAsync(requestUrl, ct)
                .ContinueWith(t =>
                {
                    if (t.IsCanceled)
                        return;
                    WriteDebug($"request1 finished with result: {t.Result.StatusCode}");
                });
        }

        private Task ConfigureTask2(string requestUrl, CancellationToken ct)
        {
            return HttpClient.GetAsync(requestUrl, ct)
                .ContinueWith(t =>
                {
                    if (t.IsCanceled)
                        return;
                    WriteDebug($"request2 finished with result: {t.Result.StatusCode}");
                });
        }

        private Task ConfigureTask3(string requestUrl, CancellationToken ct)
        {
            var task3Stopwatch = new Stopwatch();
            task3Stopwatch.Start();
            return HttpClient.GetAsync(requestUrl, ct)
                .ContinueWith(t => Task.Run(() =>
                {
                    if (t.IsCanceled)
                        return;
                    task3Stopwatch.Stop();
                    WriteDebug($"request3 finished with result: {t.Result.StatusCode}");
                    _task3MetricsCollector.AddMetrics(task3Stopwatch.ElapsedMilliseconds);
                    WriteDebug($"request3 elapsed: {task3Stopwatch.ElapsedMilliseconds}ms");
                }));
        }

        private Task ConfigureTask4And5(string request4Url, string request5Url, CancellationToken ct)
        {
            return HttpClient.GetAsync(request4Url, ct)
                .ContinueWith(t =>
                {
                    if (t.IsCanceled)
                        return Task.FromResult(0);
                    WriteDebug($"request4 finished with result: {t.Result.StatusCode}");
                    return t.Result.StatusCode == System.Net.HttpStatusCode.OK
                        ? HttpClient.GetAsync(request5Url, ct)
                            .ContinueWith(tt =>
                            {
                                if (tt.IsCanceled)
                                    return;
                                WriteDebug($"request5 finished with result: {t.Result.StatusCode}");
                            })
                        : Task.Delay(1);
                });
        }

        private void WriteDebug(string message)
        {
            _logger.Debug(message);
            Debug.WriteLine(message);
        }
    }
}
