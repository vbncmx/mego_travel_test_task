﻿using MegoTravelTestTask.Services;
using NLog;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;

namespace MegoTravelTestTask.Controllers
{
    public class RequestController : ApiController
    {
        // todo: добавить зависимость на IDelayProvider в конструктор
        private readonly IDelayProvider _delayProvider;

        private static readonly Random Random = new Random();

        public RequestController()
        {
            _delayProvider = new DelayProvider();
        }

        public async Task<object> Get(int id)
        {
            var delayMs = _delayProvider.GetDelayMs(id);

            await Task.Delay(delayMs);

            if (Random.Next(2) == 0)
            {
                return Ok();
            }

            throw new Exception("not ok");
        }
    }
}