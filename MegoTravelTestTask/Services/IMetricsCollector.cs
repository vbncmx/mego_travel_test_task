﻿namespace MegoTravelTestTask.Services
{
    public interface IMetricsCollector
    {
        void AddMetrics(long delayMs);
    }
}
