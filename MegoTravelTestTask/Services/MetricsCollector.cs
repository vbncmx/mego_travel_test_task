﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MegoTravelTestTask.Services
{
    public class MetricsCollector : IMetricsCollector
    {
        private static readonly Dictionary<long, int> _delayDictionary = new Dictionary<long, int>();
        private static object _delayDictionaryLock = new object();

        public void AddMetrics(long delayMs)
        {
            lock (_delayDictionaryLock)
            {
                if (_delayDictionary.ContainsKey(delayMs))
                {
                    _delayDictionary[delayMs] += 1;
                }
                else
                {
                    _delayDictionary[delayMs] = 1;
                }
            }
        }
    }
}