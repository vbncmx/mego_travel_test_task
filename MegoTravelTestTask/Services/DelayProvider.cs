﻿using System;

namespace MegoTravelTestTask.Services
{
    public class DelayProvider : IDelayProvider
    {
        private static readonly Random Random = new Random();

        public int GetDelayMs(int requestId)
        {
            switch(requestId)
            {
                case 1:
                    return 10000;
                case 2:
                    return 7000;
                case 3:
                    return Random.Next(1000, 20000);
                case 4:
                    return 3000;
                default:
                    return 6000;
            }
        }
    }
}