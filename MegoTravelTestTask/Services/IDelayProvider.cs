﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegoTravelTestTask.Services
{
    public interface IDelayProvider
    {
        int GetDelayMs(int requestId);
    }
}
